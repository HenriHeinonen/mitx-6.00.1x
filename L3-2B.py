# https://courses.edx.org/courses/MITx/6.00.1x_5/1T2015/courseware/Week_2/videosequence:Lecture_3/
# Convert the following into code that uses a while loop.
# print Hello!
# print 10
# print 8
# print 6
# print 4
# print 2
laskuri = 10
while(True):
    if (laskuri == 10):
       print "Hello!"
    elif (laskuri < 2):
       break
    print laskuri
    laskuri -= 2