# https://courses.edx.org/courses/MITx/6.00.1x_5/1T2015/courseware/Week_2/videosequence:Lecture_4/
# Write a Python function, square, that takes in one number and returns the square of that number.
# This function takes in one number and returns one number.
def square(x):
    '''
    x: int or float.
    '''
    # Your code here
    return x*x