# https://courses.edx.org/courses/MITx/6.00.1x_5/1T2015/courseware/Week_2/videosequence:Lecture_3/
# In this problem, you'll create a program that guesses a secret number!
# The program works as follows: you (the user) thinks of an integer between 0 (inclusive) and 100 (not inclusive). The computer makes guesses, and you give it input - is its guess too high or too low? Using bisection search, the computer will guess the user's secret number!
print "Please think of a number between 0 and 100!"
left = 0
right = 100
guess = (right-left)/2 + left
while (True):
    print "Is your secret number ",
    print guess,
    print "?"
    s = raw_input("Enter 'h' to indicate the guess is too high. Enter 'l' to indicate the guess is too low. Enter 'c' to indicate I guessed correctly. ")
    if (s == "c"):
        print "Game over. Your secret number was: ",
        print guess
        break
    elif (s == "h"):
        right = guess
        guess = (right-left)/2 + left
    elif (s == "l"):
        left = guess
        guess = (right-left)/2 + left
    else:
        print "Sorry, I did not understand your input."