# https://courses.edx.org/courses/MITx/6.00.1x_5/1T2015/courseware/Week_2/videosequence:Lecture_3/
# Write a for loop that sums the values 1 through end, inclusive. end is a variable that we define for you. So, for example, if we define end to be 6, your code should print out the result:
# 21
# which is 1 + 2 + 3 + 4 + 5 + 6.
summa = 0
for laskuri in range(1,end+1):
    summa+=laskuri
print summa