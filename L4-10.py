# https://courses.edx.org/courses/MITx/6.00.1x_5/1T2015/courseware/Week_2/videosequence:Lecture_4/
# Define a function isVowel(char) that returns True if char is a vowel ('a', 'e', 'i', 'o', or 'u'), and False otherwise. You can assume that char is a single letter of any case (ie, 'A' and 'a' are both valid).
# Do not use the keyword in. Your function should take in a single string and return a boolean.
def isVowel(char):
    '''
    char: a single letter of any case

    returns: True if char is a vowel and False otherwise.
    '''
    # Your code here
    return ( str(char).lower() == 'a' or str(char).lower() == 'e' or str(char).lower() == 'i' or str(char).lower() == 'o' or str(char).lower() == 'u' )