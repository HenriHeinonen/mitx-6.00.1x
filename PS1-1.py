# https://courses.edx.org/courses/MITx/6.00.1x_5/1T2015/courseware/Week_2/Basic_Problem_Set_1/
# Assume s is a string of lower case characters.
# Write a program that counts up the number of vowels contained in the string s. Valid vowels are: 'a', 'e', 'i', 'o', and 'u'. For example, if s = 'azcbobobegghakl', your program should print:
# Number of vowels: 5
 # Paste your code into this box 
s = 'azcbobobegghakl'

def countVowels(s):
   numVowels = 0
   for char in s:
      if (char=='a' or char=='e' or char=='i' or char=='o' or char=='u'):
         numVowels = numVowels + 1
   return numVowels

print "Number of vowels: ", countVowels(s)