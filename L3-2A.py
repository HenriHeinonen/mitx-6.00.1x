# https://courses.edx.org/courses/MITx/6.00.1x_5/1T2015/courseware/Week_2/videosequence:Lecture_3/
# Convert the following into code that uses a while loop.
# print 2
# print 4
# print 6
# print 8
# print 10
# print Goodbye!
laskuri = 2
while (laskuri <= 10):
    print laskuri
    laskuri += 2
print "Goodbye!"