# https://courses.edx.org/courses/MITx/6.00.1x_5/1T2015/courseware/Week_2/videosequence:Lecture_4/
# Write a Python function, odd, that takes in one number and returns True when the number is odd and False otherwise.
# You should use the % (mod) operator, not if.
# This function takes in one number and returns a boolean.
def odd(x):
    '''
    x: int or float.

    returns: True if x is odd, False otherwise
    '''
    # Your code here
    return x%2!=0