# https://courses.edx.org/courses/MITx/6.00.1x_5/1T2015/courseware/Week_2/Basic_Problem_Set_1/
# Assume s is a string of lower case characters.
# Write a program that prints the number of times the string 'bob' occurs in s. For example, if s = 'azcbobobegghakl', then your program should print
# Number of times bob occurs is: 2
 # Paste your code into this box 
s = 'azcbobobegghakl'
indeksiLaskuri = 0
laskuri = 0
muisti = 0

for char in s:
    print s[indeksiLaskuri]
    if ( (s.find("bob",indeksiLaskuri) != muisti) and (s.find("bob",indeksiLaskuri) != -1) ):
        laskuri+=1
        muisti = s.find("bob",indeksiLaskuri)
    indeksiLaskuri += 1

print "Number of bobs: ", laskuri